From sphinxdoc/sphinx
RUN apt-get update
RUN apt-get install -y gcc 

COPY modules/dyrkdevice/requirements.txt /tmp/pip-tmp/
COPY modules/dyrkhost/requirements.txt /tmp/pip-tmp/requirements2.txt
RUN pip3 install -r /tmp/pip-tmp/requirements.txt
RUN pip3 install -r /tmp/pip-tmp/requirements2.txt
RUN pip3 install --upgrade myst-parser sphinx_rtd_theme