.. Dyrk documentation master file, created by
   sphinx-quickstart on Sun Aug  1 12:50:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dyrk's documentation!
================================
Dyrk is a project aiming to grow biology. The goal is to automate the growth of different biology, and to help people learn and experiment with different growth settings.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   intro.md
   yocto.md
   modules

   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
