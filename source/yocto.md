# Yocto (raspberry pi)
The host system is running a custom linux created with yocto.  
The yocto is very minimal, and have the following features:

 - Connectivity via wifi, ethernet or uart
 - Python3 and pip3
 - Nodejs
 - Support for vscode remote ssh
 - Docker

## getting started
First of all, you need to download the image and flash it to a SD-card.  
The latest image is availible here: http://dl.dyrk.io/

### Special case: compiling an image with wifi settings
There are some cases when one would want to compile an image with a set wifi ssid and password.
If you need to do this, you can follow theese simple steps:

```
git clone --recursive git@gitlab.com:dyrk/yocto/builder.git
cd builder
./enter-docker.sh
. oe-init-build-env
export wpa_ssid="SSID"
export wpa_pwd="wifiPassword"
export BB_ENV_EXTRAWHITE="$BB_ENV_EXTRAWHITE wpa_ssid wpa_pwd"
bitbake dyrk-image-dev
```



